"use strict";
//constructor
function saper(){
	//property
	var self = this; 
	var WidthBoard; 
	var HeightBoard; 
	var BombCount; 
	var mapMine = [];
	//methods

	
	self.init = function(options){
		WidthBoard = options ? options.WidthBoard : 20;
		HeightBoard = options ? options.HeightBoard : 20;
		BombCount = options ? options.BombCount : 5; 
		
		gui();
	}
	
	self.getter = function(){
		console.log(mapMine);
		console.log('WidthBoard: ' + WidthBoard + ';', 'HeightBoard: ' + HeightBoard + ';',
																 'BombCount: ' + BombCount + ';');
		console.log('randWidthBoard: ' + rand(WidthBoard)+ ';', 'randHeightBoard: ' + rand(HeightBoard)+ ';');
	}

	function gui(){

		createMapMine();

		var divMap = document.createElement('div');
		divMap.className = 'map';

		for (var i=0; i!=WidthBoard; i++){
			for (var j=0; j!=HeightBoard; j++){
				var divCell = document.createElement('div');
				divCell.className = 'cell';
				divCell.setAttribute('data-x', i);
				divCell.setAttribute('data-y',j);
				
				divMap.appendChild(divCell);			
			}
		}
		
		$('body').append(divMap);

		var visualMap = $('div.map');
		var divCellW = $('div.cell').first().width()+1;
		var divMapW = divCellW*WidthBoard;
		visualMap.css({'width':divMapW + 'px'});

		visualMap.on('click', function(e){
			var target = e.target;

			if (target.innerHTML == '!') return
			if (target.className == 'cell'){
				var valueTarget = mapMine[+target.getAttribute('data-x')][+target.getAttribute('data-y')];

				if(valueTarget > 0) openCell(target);
				else roll(+target.getAttribute('data-x'), +target.getAttribute('data-y'));

				if(valueTarget == 'bomb') gameOver();
			}
		});

		visualMap.on('contextmenu', function(e){
			var target = e.target;

			if (target.className == 'cell'){
				if((target.textContent!= '!') && (!target.getAttribute('id')))
					target.innerHTML = '!';
				else target.innerHTML = '';
			}

			return false;
		}); 
		
	}

	function openCell(elem){
		var x = +elem.getAttribute('data-x'),
			y = +elem.getAttribute('data-y');
		var	id = mapMine[x][y] === 'bomb'? 'mineCell' : 'clearCell' ;
			elem.setAttribute('id', id);
		if ((id !== 'bomb') && (mapMine[x][y] > 0)){
				if(!elem.textContent)
				elem.innerHTML = mapMine[x][y];
		}
		
	}
	function roll(x, y){

		if(x < 0 || y < 0 || x >= WidthBoard || y >= HeightBoard){
			return
		}

		var elem = document.querySelector('div.map div.cell[data-y = "'+ y +'"][data-x = "'+ x +'"]');

		if (elem.innerHTML == '!') return

		if(elem.getAttribute('data-checked')) return

		openCell(elem);

		if((mapMine[x][y]!== 'bomb') && (mapMine[x][y] > 0))
			return		

		elem.setAttribute('data-checked', 'true');

		roll(x+1, y);
		roll(x-1, y);
		roll(x, y+1);
		roll(x, y-1);
		roll(x-1,y-1);
		roll(x+1, y-1);
		roll(x-1, y+1);
		roll(x+1, y+1);
	}
	
	function createMapMine(){
		var countPlacedBomb = 0;
		var x, y;
		for(var i = 0; i < WidthBoard; i++){
			mapMine[i] = [];
			for(var j = 0; j < HeightBoard; j++){
				mapMine[i][j] = 0;				
			}
		}
			
		while(countPlacedBomb != BombCount){
			x = rand(WidthBoard);
			y = rand(HeightBoard);
			if(mapMine[x][y] !='bomb'){
				mapMine[x][y] = 'bomb';
				valueOfAboutMine(x, y);
				countPlacedBomb++;
			}			
		}
	}
	
	function valueOfAboutMine(CONST_X, CONST_Y){
		var StartX, StartY;
		var onUpToBottom, onLeftToRight;
		var x, y, i = 0, j = 0;
		//start coordinate
		if(CONST_X == 0){ 
				StartX = ++CONST_X;
				onUpToBottom = false;

			}else{
				StartX = --CONST_X;
				onUpToBottom = true;
			}

			if(CONST_Y == 0){
				StartY = ++CONST_Y ; 
				onLeftToRight = false;
			}else{
				StartY = --CONST_Y;
				onLeftToRight = true;
			}

		x = StartX;
		y = StartY;

		while(i<3){
			j=0;
			while(j<3){						
				if((x>=0) && (x<=WidthBoard-1) && (y>=0) && (y<=HeightBoard-1) && (mapMine[x][y] != 'bomb')) mapMine[x][y]+=1;						
				if(onLeftToRight) y++; else y--;
				j++;
			}

			if(onUpToBottom) x++; else x--;
			y=StartY;	
			i++;
		}
	}
	function gameOver(){
		var newGame = confirm('You Lose!!! Start a new game?');
		if(newGame) location.reload();
	}
	//random coordinates mines accommodation
	function rand(max){
		return Math.floor(Math.random()* ((--max) - 0 + 1)) + 0;
	}

}
